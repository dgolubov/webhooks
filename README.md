# Webhooks

Webhooks to integrate CERN services to user pods in Kubeflow:
- EOS
- Nvidia drivers
- Kerberos credentials

## Deployment

### Generate Certificates

Do the following locally: (important - make sure **hostname** is equal to .webhooks.name in webhooks.yaml)
```
git clone https://github.com/marcel-dempers/docker-development-youtube-series.git

cd kubernetes\admissioncontrollers\introduction

docker run -it --rm -v ${PWD}:/work -w /work debian bash

apt-get update && apt-get install -y curl &&
curl -L https://github.com/cloudflare/cfssl/releases/download/v1.5.0/cfssl_1.5.0_linux_amd64 -o /usr/local/bin/cfssl && \
curl -L https://github.com/cloudflare/cfssl/releases/download/v1.5.0/cfssljson_1.5.0_linux_amd64 -o /usr/local/bin/cfssljson && \
chmod +x /usr/local/bin/cfssl && \
chmod +x /usr/local/bin/cfssljson

#generate ca in /tmp
cfssl gencert -initca ./tls/ca-csr.json | cfssljson -bare /tmp/ca

#generate certificate in /tmp
cfssl gencert \
  -ca=/tmp/ca.pem \
  -ca-key=/tmp/ca-key.pem \
  -config=./tls/ca-config.json \
  -hostname="cern-webhook,cern-webhook.kubeflow.svc.cluster.local,cern-webhook.kubeflow.svc,localhost,127.0.0.1" \
  -profile=default \
  ./tls/ca-csr.json | cfssljson -bare /tmp/cern-webhook

#make a secret
cat <<EOF > ./tls/cern-webhook-tls.yaml
apiVersion: v1
kind: Secret
metadata:
  name: cern-webhook-tls
type: Opaque
data:
  tls.crt: $(cat /tmp/cern-webhook.pem | base64 | tr -d '\n')
  tls.key: $(cat /tmp/cern-webhook-key.pem | base64 | tr -d '\n') 
EOF

#generate CA Bundle + inject into template
ca_pem_b64="$(openssl base64 -A <"/tmp/ca.pem")"

sed -e 's@${CA_PEM_B64}@'"$ca_pem_b64"'@g' <"webhook-template.yaml" \
    > webhook.yaml
```
Obtain values from local `cern-webhook-tls.yaml` to `cern-webhook-tls.yaml`

### Create Kuberenetes Resources

```
kubectl -n kubeflow apply -f cern-webhook-tls.yaml
```

```
kubectl -n kubeflow apply -f rbac.yaml
```

```
kubectl -n kubeflow apply -f deployment.yaml
```

```
kubectl -n kubeflow apply -f webhook.yaml
```

### Test

Create a demo pod:

```
kubectl -n kubeflow apply -f demo_pod.yaml
```

Check if it has a label `"example-webhook": "it-worked"`

```
kubectl -n kubeflow get pods demo-pod -ojsonpath="{.metadata.labels}" | jq
```
